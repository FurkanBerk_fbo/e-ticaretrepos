var Database_Name = 'eticaretdata';
var Version = 1.0;
var Text_Description = 'E-Ticaret Otomasyon proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

var selectadi, selectmarkasi, selectmodeli, selectfiyati, selectstogu, sepeteekle, selectimg
var len
var secilenurunno, girenuser

if (localStorage.getItem("user") == null) {
    window.location.href = "/loginekran/login.html";
} else {
    girenuser = JSON.parse(localStorage.getItem("user")).personid
        // var deger = JSON.parse(localStorage.getItem("user")).id
        // if (!(deger == 0 || deger == 1)) {
        //     window.location.href = "/loginekran/login.html";
        // }
}

var anasayfaservice = ""

function anasayfaInit() {

    anasayfaservice = new AnasayfaClass();

    urunsirala()


}

function urunsirala() {

    // var ansrefselect = new ansyfSelectProduct()
    urunalani.innerHTML = ""


    anasayfaservice.selecturun().then(function(res) {


        len = res
        length = res.length
        var i
        for (i = 0; i < length; i++) {
            selectid = res.item(i).rowid
            selectno = res.item(i).urunno
            selectadi = res.item(i).urunadi
            selectmarkasi = res.item(i).urunmarkasi
            selectstogu = res.item(i).urunstogu
            selectmodeli = res.item(i).urunmodeli
            selectfiyati = res.item(i).urunfiyati
            selectimg = res.item(i).urunimg

            var urunkart = document.createElement("div");
            var resim = document.createElement("img")
            resim.className = "rounded "
            resim.width = 200;
            resim.height = 150;
            resim.setAttribute("src", selectimg)
            sepeteekle = document.createElement("button")
            sepeteekle.setAttribute("id", "btn" + i)
            sepeteekle.innerHTML = "&emsp;SEPETE EKLE"
            sepeteekle.className = " fas fa-check-square btn btn-success my-3"
            urunkart.className = "col-3 offset-1 bg-light text-lg-center float-left mt-4 border border-info rounded text-primary m-5 "
                // urunkart.setAttribute("class", "urunsira")
            sepeteekle.setAttribute("onclick", "sepeteat(" + i + ")")





            urunkart.innerHTML = selectadi + " , " + selectmarkasi + " , " + selectmodeli + " <hr>" + "Ürün fiyat  : " + selectfiyati + "<hr>" + "Ürün stok  : " + selectstogu + "<hr>";

            urunkart.appendChild(resim)
            urunalani.appendChild(urunkart);
            urunkart.appendChild(sepeteekle)

        }
    })

}

function sepeteat(index) {
    // var ansrefexist = new ansyfExistsBasket()
    // var ansrefupdate = new ansyfUpdateBasket()
    // var ansrefadd = new ansyfAddBasket()
    secilenurunno = len.item(index).urunno
    var surunad = len.item(index).urunadi
    var surunmarka = len.item(index).urunmarkasi
    var surunmodel = len.item(index).urunmodeli
    var surunfiyat = len.item(index).urunfiyati
    var surunstok = 1 //len.item(index).urunstogu
    var surunimg = len.item(index).urunimg

    anasayfaservice.SepetteVarmi(secilenurunno, girenuser).then(function(tx) {


        if (tx.length > 0) {
            var sayia = Number(tx.item(0).urunstogu)


            sayia += 1

            anasayfaservice.SepetGuncelle(sayia, secilenurunno).then(function(res) {
                if (res) {
                    console.log("BAŞARILI ADET GÜNCELLEME")
                } else {
                    console.log("HATALI ADET GÜNCELLEME")
                }


            })

        } else {
            debugger
            anasayfaservice.SepeteEkle(girenuser, secilenurunno, surunad, surunmarka, surunstok, surunmodel, surunfiyat, surunimg).then(function(res) {
                if (res) {
                    alert("BAŞARILI SEPETE EKLEME")
                } else {
                    alert("HATALI SEPETE EKLEME")
                }
            })
        }
    })


}

function sepetegit() {
    window.location.href = "/sepetekran/sepet.html";

}

function logingit() {
    localStorage.clear()
    window.location.href = "/loginekran/login.html";

}

function sepetsil() {
    // var ansrefdelete = new ansyfDeleteBAsket()
    anasayfaservice.deletebutunsepet().then(function(res) {
        if (res) {
            alert("BAŞARILI SEPET SİLME")
            window.location.href = "/loginekran/login.html";

        } else {
            alert("HATALIU SEPET SİLME")
        }
    })

}