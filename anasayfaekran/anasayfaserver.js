var Database_Name = 'eticaretdata';
var Version = 1.0;
var Text_Description = 'E-Ticaret Otomasyon proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

// urunno,urunadi,urunmarkasi,urunstogu,urunmodeli,urunfiyati
// class ansyfSelectProduct {
class AnasayfaClass {

    selecturun() {

            var liste = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('SELECT *from  UrunlerTablo ', [],
                        function(tx, result) {

                            resolve(result.rows)
                        },
                        function(tx, error) {
                            resolve(false)
                            console.log("urun secimi başarısız")
                        })
                })
            });
            return liste;



        }
        // }

    // class ansyfAddBasket {
    SepeteEkle(girenid, urunno, urunad, urunmarka, urunstok, urunmodel, urunfiyat, urunimg) {

            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("INSERT INTO SepetTablo(girenid,urunno,urunadi,urunmarkasi,urunstogu,urunmodeli,urunfiyati,urunimg) VALUES(?,?,?,?,?,?,?,?)", [girenid, urunno, urunad, urunmarka, urunstok, urunmodel, urunfiyat, urunimg],
                        function(tx, result) {
                            resolve(true)
                            console.log("ürün sepete eklendi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ürün sepete başarısız ekleme")
                        })
                })
            });
            return urun;
        }
        // }

    // class ansyfDeleteBAsket {
    deletebutunsepet() {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('delete from SepetTablo', [],
                        function(tx, result) {
                            resolve(true)
                            console.log("sepetteki ürünler silindi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ürünler başarısız silindi")
                        })
                })
            });
            return urun;

        }
        // }

    // class ansyfExistsBasket {
    SepetteVarmi(eklenenno, kullanici) {

            var liste = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('SELECT *From  SepetTablo where urunno=? and girenid=?', [eklenenno, kullanici],
                        function(tx, result) {

                            resolve(result.rows)
                        },
                        function(tx, error) {
                            resolve(false)
                            console.log("urun secimi başarısız")
                        })
                })
            });
            return liste;



        }
        // }

    // class ansyfUpdateBasket {
    SepetGuncelle(urunadet, urunno) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("UPDATE SepetTablo  set urunstogu=? where urunno=?", [urunadet, urunno],
                        function(tx, result) {
                            resolve(true)
                            console.log("sepetadet güncellendi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("sepetadet guncel hatası")
                        })
                })
            });
            return urun;


        }
        // }
}