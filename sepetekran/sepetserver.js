var Database_Name = 'eticaretdata';
var Version = 1.0;
var Text_Description = 'E-Ticaret Otomasyon proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

// class sptCreatBasketTable {
class SepetClass {
    creatSepetTablo() {

            var urun = new Promise(function(resolve) {

                veriler.transaction(function(calis) {
                    calis.executeSql('CREATE TABLE IF NOT EXISTS SepetTablo(girenid,urunno ,urunadi,urunmarkasi ,urunstogu,urunmodeli,urunfiyati,urunimg)', [],
                        function(tx, result) {
                            resolve(true)
                            console.log("SEPET TABLO OLUŞTURULDU")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("SEPET TABLOSU OLUŞTURULAMADI")
                        })
                })
            });
            return urun;

        }
        // }

    // class sptSelectBasketProduct {
    selectsepeturun() {
            var selectkisiurun = JSON.parse(localStorage.getItem("user")).personid
            var liste = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('SELECT *From  SepetTablo where girenid=?', [selectkisiurun],
                        function(tx, result) {

                            resolve(result.rows)
                        },
                        function(tx, error) {
                            resolve(false)
                            console.log(" ÜRÜN SEÇİMİ BAŞARISIZ")
                        })
                })
            });
            return liste;



        }
        // }

    // class sptDeleteBasketProduct {
    deletesepetUrun(urunnosu) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('delete from SepetTablo where urunno=?', [urunnosu],
                        function(tx, result) {
                            resolve(true)
                            console.log("ÜRÜN SEPETTEN  SİLİNDİ SERVER")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ÜRÜN BAŞARISIZ SİLME SERVER")
                        })
                })
            });
            return urun;

        }
        // }

    // class sptUpdateBasket {
    sepetguncelle(urunadet, urunno) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("UPDATE SepetTablo  set urunstogu=? where urunno=?", [urunadet, urunno],
                        function(tx, result) {
                            resolve(true)
                            console.log("SEPET ADETİ GÜNCELLENDİ SERVER")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("SEPET GÜNCELLEME HATASI SERVER")
                        })
                })
            });
            return urun;


        }
        // }

    // class sptSelectProductTable {
    UrunTabloSec(arananno) {

            var liste = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('SELECT *from  UrunlerTablo where urunno=? ', [arananno],
                        function(tx, result) {

                            resolve(result.rows)
                        },
                        function(tx, error) {
                            resolve(false)
                            console.log("ÜRÜN SEÇİMİ BAŞARISIZ SERVER")
                        })
                })
            });
            return liste;



        }
        // }

    // class sptProductTableUpdate {
    UrunTabloguncelle(urunstok, urunno) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("UPDATE UrunlerTablo  set urunstogu=? where urunno=?", [urunstok, urunno],
                        function(tx, result) {
                            resolve(true)
                            console.log("ÜRÜN STOK DOĞRU GÜNCELLENDİ SERVER")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ÜRÜN STOK GÜNCELLENMEDİ HATA SERVER")
                        })
                })
            });
            return urun;


        }
        // }
        // class sptDeleteAllBaket {
    deletebutunsepet() {
        var deletekisiurun = JSON.parse(localStorage.getItem("user")).personid
        var urun = new Promise(function(resolve) {

            veriler.transaction(function(tx) {
                tx.executeSql('delete from SepetTablo where girenid=?', [deletekisiurun],
                    function(tx, result) {
                        resolve(true)
                        console.log("SEPETTEKİ ÜRÜNLER SİLİNDİ SERVER")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("SEPET HATALI ÜRÜN SİLME SERVER")
                    })
            })
        });
        return urun;

    }
}
// }


// var Database_Name = 'eticaretdata';
// // var Version = 1.0;
// // var Text_Description = 'E-Ticaret Otomasyon proje';
// // var Database_Size = 2 * 1024 * 1024;
// // var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

// // function creatSepetTablo() {

// //     var urun = new Promise(function(resolve) {

// //         veriler.transaction(function(calis) {
// //             calis.executeSql('CREATE TABLE IF NOT EXISTS SepetTablo(urunno ,urunadi,urunmarkasi ,urunstogu,urunmodeli,urunfiyati,urunimg)', [],
// //                 function(tx, result) {
// //                     resolve(true)
// //                     console.log("sepet tablo oluşturuldu")

// //                 },
// //                 function(tx, result) {
// //                     resolve(false)
// //                     console.log("sepet tablo oluşturlmadı")
// //                 })
// //         })
// //     });
// //     return urun;

// // }

// // function selectsepeturun() {

// //     var liste = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql('SELECT *from  SepetTablo ', [],
// //                 function(tx, result) {
// //                  
// //                     resolve(result.rows)
// //                 },
// //                 function(tx, error) {
// //                     resolve(false)
// //                     console.log("urun secimi başarısız")
// //                 })
// //         })
// //     });
// //     return liste;



// // }

// // function deletesepetUrun(urunnosu) {
// //     var urun = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql('delete from SepetTablo where urunno=?', [urunnosu],
// //                 function(tx, result) {
// //                     resolve(true)
// //                     console.log("ürün silindi")

// //                 },
// //                 function(tx, result) {
// //                     resolve(false)
// //                     console.log("ürün başarısız silindi")
// //                 })
// //         })
// //     });
// //     return urun;

// // }

// // function sepetguncelle(urunadet, urunno) {
// //     var urun = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql("UPDATE SepetTablo  set urunstogu=? where urunno=?", [urunadet, urunno],
// //                 function(tx, result) {
// //                     resolve(true)
// //                     console.log("sepetadet güncellendi")

// //                 },
// //                 function(tx, result) {
// //                     resolve(false)
// //                     console.log("sepetadet guncel hatası")
// //                 })
// //         })
// //     });
// //     return urun;


// // }

// // function UrunTabloSec() {

// //     var liste = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql('SELECT *from  UrunlerTablo ', [],
// //                 function(tx, result) {
// //                    
// //                     resolve(result.rows)
// //                 },
// //                 function(tx, error) {
// //                     resolve(false)
// //                     console.log("urun secimi başarısız")
// //                 })
// //         })
// //     });
// //     return liste;



// // }

// // function UrunTabloguncelle(urunstok, urunno) {
// //     var urun = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql("UPDATE UrunlerTablo  set urunstogu=? where urunno=?", [urunstok, urunno],
// //                 function(tx, result) {
// //                     resolve(true)
// //                     console.log("UrunStok güncellendi")

// //                 },
// //                 function(tx, result) {
// //                     resolve(false)
// //                     console.log("UrunStok guncel hatası")
// //                 })
// //         })
// //     });
// //     return urun;


// // }

// // function deletebutunsepet() {
// //     var urun = new Promise(function(resolve) {

// //         veriler.transaction(function(tx) {
// //             tx.executeSql('delete from SepetTablo', [],
// //                 function(tx, result) {
// //                     resolve(true)
// //                     console.log("sepetteki ürünler silindi")

// //                 },
// //                 function(tx, result) {
// //                     resolve(false)
// //                     console.log("ürünler başarısız silindi")
// //                 })
// //         })
// //     });
// //     return urun;

// // }
// var Database_Name = 'eticaretdata';
// var Version = 1.0;
// var Text_Description = 'E-Ticaret Otomasyon proje';
// var Database_Size = 2 * 1024 * 1024;
// var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);

// function creatSepetTablo() {

//     var urun = new Promise(function(resolve) {

//         veriler.transaction(function(calis) {
//             calis.executeSql('CREATE TABLE IF NOT EXISTS SepetTablo(urunno ,urunadi,urunmarkasi ,urunstogu,urunmodeli,urunfiyati,urunimg)', [],
//                 function(tx, result) {
//                     resolve(true)
//                     console.log("sepet tablo oluşturuldu")

//                 },
//                 function(tx, result) {
//                     resolve(false)
//                     console.log("sepet tablo oluşturlmadı")
//                 })
//         })
//     });
//     return urun;

// }

// function selectsepeturun() {

//     var liste = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql('SELECT *from  SepetTablo ', [],
//                 function(tx, result) {
//                     
//                     resolve(result.rows)
//                 },
//                 function(tx, error) {
//                     resolve(false)
//                     console.log("urun secimi başarısız")
//                 })
//         })
//     });
//     return liste;



// }

// function deletesepetUrun(urunnosu) {
//     var urun = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql('delete from SepetTablo where urunno=?', [urunnosu],
//                 function(tx, result) {
//                     resolve(true)
//                     console.log("ürün silindi")

//                 },
//                 function(tx, result) {
//                     resolve(false)
//                     console.log("ürün başarısız silindi")
//                 })
//         })
//     });
//     return urun;

// }

// function sepetguncelle(urunadet, urunno) {
//     var urun = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql("UPDATE SepetTablo  set urunstogu=? where urunno=?", [urunadet, urunno],
//                 function(tx, result) {
//                     resolve(true)
//                     console.log("sepetadet güncellendi")

//                 },
//                 function(tx, result) {
//                     resolve(false)
//                     console.log("sepetadet guncel hatası")
//                 })
//         })
//     });
//     return urun;


// }

// function UrunTabloSec() {

//     var liste = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql('SELECT *from  UrunlerTablo ', [],
//                 function(tx, result) {
//                     
//                     resolve(result.rows)
//                 },
//                 function(tx, error) {
//                     resolve(false)
//                     console.log("urun secimi başarısız")
//                 })
//         })
//     });
//     return liste;



// }

// function UrunTabloguncelle(urunstok, urunno) {
//     var urun = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql("UPDATE UrunlerTablo  set urunstogu=? where urunno=?", [urunstok, urunno],
//                 function(tx, result) {
//                     resolve(true)
//                     console.log("UrunStok güncellendi")

//                 },
//                 function(tx, result) {
//                     resolve(false)
//                     console.log("UrunStok guncel hatası")
//                 })
//         })
//     });
//     return urun;


// }

// function deletebutunsepet() {
//     var urun = new Promise(function(resolve) {

//         veriler.transaction(function(tx) {
//             tx.executeSql('delete from SepetTablo', [],
//                 function(tx, result) {
//                     resolve(true)
//                     console.log("sepetteki ürünler silindi")

//                 },
//                 function(tx, result) {
//                     resolve(false)
//                     console.log("ürünler başarısız silindi")
//                 })
//         })
//     });
//     return urun;

// }