var len
var selectid, selectno, selectadi, selectmarkasi, selelectstogu, selectmodeli, selectfiyati, selectimg
var urunalani = document.getElementById("urunler")
var sayi

var urunstok, secilenstok, stok = 100,
    secilenurun;

if (localStorage.getItem("user") == null) {
    window.location.href = "/loginekran/login.html";
}

var sepetservice = ""

function sepetInit() {
    sepetservice = new SepetClass();

    tablogetir()
    sepeturunsirala()
}

function tablogetir() {
    // var sptrefcreat = new sptCreatBasketTable()
    sepetservice.creatSepetTablo()
}

function sepeturunsirala() {
    // var sptrefselect = new sptSelectBasketProduct()
    urunalani.innerHTML = ""

    sepetservice.selectsepeturun().then(function(res) {

        // console.log(res[1].urunadi)
        // alert(res.item(1).urunadi)
        len = res
        length = res.length
        var i
        var top = 0;
        for (i = 0; i < length; i++) {
            selectid = res.item(i).rowid
            selectno = res.item(i).urunno
            selectadi = res.item(i).urunadi
            selectmarkasi = res.item(i).urunmarkasi
            selectstogu = res.item(i).urunstogu
            selectmodeli = res.item(i).urunmodeli
            selectfiyati = res.item(i).urunfiyati
            selectimg = res.item(i).urunimg

            var urunkart = document.createElement("div");
            var resim = document.createElement("img")
            resim.setAttribute("src", selectimg)
            resim.className = "rounded "
            resim.width = 200;
            resim.height = 150;
            stoktext = document.createElement("p")
            stoktext.setAttribute("id", "p")
            urunsill = document.createElement("button")
            urunsill.setAttribute("id", "btn" + i)
            urunsill.innerHTML = "&emsp;ÜRÜN SİL"
            urunsill.className = " fas fa-trash-alt btn btn-danger my-2"
                // urunkart.setAttribute("class", "urunsira")
            urunkart.className = "col-3 offset-1 bg-light text-lg-center float-left m-4 border border-success rounded text-primary "
            urunsill.setAttribute("onclick", "sepeturunsil(" + i + ")")
            arttir = document.createElement("button")
            arttir.innerHTML = "&emsp;ARTTIR"
            arttir.className = " fas fa-angle-double-up btn-sm btn-success my-2  ml-1"
            arttir.setAttribute("onclick", "sepetarttir(" + i + ")")
            azalt = document.createElement("button")
            azalt.innerHTML = "&emsp;AZALT"
            azalt.className = " fas fa-angle-double-down btn-sm btn-info my-2 "
            azalt.setAttribute("onclick", "sepetazalt(" + i + ")")




            urunkart.innerHTML = selectadi + " , " + selectmarkasi + " , " + selectmodeli + " <hr>" + "Ürün fiyat  : " + selectfiyati + "<hr>" + "Ürün ADET : " + selectstogu + "<hr>";

            urunkart.appendChild(resim)
            urunalani.appendChild(urunkart);
            urunkart.appendChild(azalt)
            urunkart.appendChild(arttir)
            urunkart.appendChild(urunsill)



            var adett = Number(len.item(i).urunstogu)
            var fiyatt = Number(len.item(i).urunfiyati)

            top += adett * fiyatt

        }
        document.getElementById("toplamfiyat").innerHTML = top + " TL "
    })



}



function sepetarttir(index) {
    // var sptrefupdate = new sptUpdateBasket()

    secilenurunno = len.item(index).urunno
    var sayia = Number(len.item(index).urunstogu)


    sayia += 1

    sepetservice.sepetguncelle(sayia, secilenurunno).then(function(res) {
        if (res) {
            console.log("DOĞRU ADET ARTTIRMA")
        } else {
            console.log("HATALI ADET ARTTIRMA")
        }


    })

    sepeturunsirala()


}

function sepetazalt(index) {

    // var sptrefupdate = new sptUpdateBasket()
    secilenurunno = len.item(index).urunno
    var sayie = Number(len.item(index).urunstogu)


    if (sayie > 1) {
        sayie -= 1
    }
    sepetservice.sepetguncelle(sayie, secilenurunno).then(function(res) {
        if (res) {
            console.log("DOĞRU ADET AZALTMA")
        } else {
            console.log("HATALI ADET AZALTMA")
        }


    })



    sepeturunsirala()


}

function sepeturunsil(index) {
    // var sptrefdelete = new sptDeleteBasketProduct()
    secilenurunno = len.item(index).urunno
    sepetservice.deletesepetUrun(secilenurunno).then(function(res) {

        if (res) {
            alert("ÜRÜN SEPETTEN SİLİNMİŞTİR")
        } else {
            alert("HATALI SİLME İŞLEMİ")
        }

    })
    sepeturunsirala()
}

function urunsayfadon() {
    window.location.href = "/anasayfaekran/anasayfa.html";

}








function uruntabloguncel() {

    // var sptreftableupdate = new sptProductTableUpdate()
    // var sptrefselecttable = new sptSelectProductTable()



    var selectedProducts = []
    for (var i = 0; i < len.length; i++) {

        selectedProducts.push(len.item(i).urunno)


    }
    var getProducts = selectedProducts.map(p => sepetservice.UrunTabloSec(p));
    var sepet = Object.assign([], len)
    var products = []
    Promise.all(getProducts).then(response => {
        console.log(response);
        products = response.map(r => Object.assign([], r)[0])

        products.forEach(p => {
            var cartProduct = sepet.find(s => s.urunno == p.urunno)
            var newStock = p.urunstogu - cartProduct.urunstogu
            console.log(p, newStock);
            sepetservice.UrunTabloguncelle(newStock, p.urunno).then(function(res) {
                if (res) {
                    console.log("DOĞRU STOK GÜNCELLEME")
                } else {
                    console.log("HATALI STOK GÜNCELLEME")
                }
            })

        })
        sepetsil()
    })



}

function sepetsil() {
    // var sptrefdeleteall = new sptDeleteAllBaket()
    sepetservice.deletebutunsepet().then(function(deger) {
        if (deger) {
            alert(" ÖDEMENİZ ALINMIŞTIR VE SEPET SİLİNMİŞTİR İYİ GÜNLER.. ")
            window.location.href = "/anasayfaekran/anasayfa.html";

        } else {
            alert("HATALI ÖDEME İŞLEMİ ")
        }
    })

}

// function uruntabloguncel() {

//     // UrunTabloSec().then(function(res) {


//     for (var i = 0; i < len.length; i++) {

//         secilenurun = len.item(i).urunno
//         
//         UrunTabloSec(secilenurun).then(function(tx) {
//             // var urun = Object.assign([], tx)

//             // stok = urun[0].urunstogu
//             stok = tx.item(0).urunstogu
//             var satilan = Number(len.item(i).urunstogu)

//             var kalanstok = stok - satilan //- Number(len.item(i).selectstogu)

//             UrunTabloguncelle(kalanstok, secilenurun).then(function(res) {
//                 if (res) {
//                     console.log("dogru kalanstok guncelleme")
//                 } else {
//                     console.log("hatalı kalanstok guncelleme")
//                 }


//             })
//         })




//     }
//     // })

//     sepetsil()


// }