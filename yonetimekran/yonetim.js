var urunalani = document.getElementById("urunler")
var sepeteekle;
var selectadi, selectmarkasi, selectmodeli, selectfiyati, selectstogu, selectno, selectimg
var uadi, umarkasi, urunstogu, urunmodeli, ufiyati
var len;
var secilenurunno, a = 0;
var deger = JSON.parse(localStorage.getItem("user")).id
if (deger == 0) {
    window.location.href = "/anasayfaekran/anasayfa.html";
}

var yonetimservice = ""

function yonetimInit() {
    yonetimservice = new yonetimClass();

    tablogetir()
    urunsirala()
}

function tablogetir() {
    // var yntrefcreat = new yntCreatProductTable()
    yonetimservice.creatUrunlerTablo();
}

function loginegit() {
    localStorage.clear()
    window.location.href = "/loginekran/login.html";
}

function urunekle() {

    // var yntrefadd = new yntAddProduct()
    var uadi = document.getElementById("urunad").value.toUpperCase();
    var umarkasi = document.getElementById("urunmarka").value.toUpperCase();
    var ustogu = document.getElementById("urunstok").value.toUpperCase();
    var umodeli = document.getElementById("urunmodel").value.toUpperCase();
    var ufiyati = document.getElementById("urunfiyat").value.toUpperCase();
    var uimg = a

    yonetimservice.addUrun(uadi, umarkasi, ustogu, umodeli, ufiyati, uimg).then(function(res) {
        if (res) {
            alert("ÜRÜN EKLEMESİ BAŞARILI")
        } else {
            alert("HATALI ÜRÜN EKLEME")
        }
    })


    urunsirala()
    document.getElementById("urunad").value = "";
    document.getElementById("urunmarka").value = "";
    document.getElementById("urunstok").value = "";
    document.getElementById("urunmodel").value = "";
    document.getElementById("urunfiyat").value = "";
    document.getElementById("imgfile").value = "";
}



function urunsirala() {
    urunalani.innerHTML = ""
        // var yntrefselect = new yntSelectProduct()
    yonetimservice.selecturun().then(function(res) {


        len = res
        length = res.length
        var i
        for (i = 0; i < length; i++) {
            selectid = res.item(i).rowid
            selectno = res.item(i).urunno
            selectadi = res.item(i).urunadi
            selectmarkasi = res.item(i).urunmarkasi
            selectstogu = res.item(i).urunstogu
            selectmodeli = res.item(i).urunmodeli
            selectfiyati = res.item(i).urunfiyati
            selectimg = res.item(i).urunimg

            var urunkart = document.createElement("div");
            var resim = document.createElement("img")
            resim.setAttribute("src", selectimg)
            resim.className = "rounded "
            resim.width = 200;
            resim.height = 150;
            sepeteekle = document.createElement("button")
            sepeteekle.setAttribute("id", "btn" + i)
            sepeteekle.innerHTML = "&emsp;ELEMAN SEÇ"
            sepeteekle.className = " fas fa-check-square btn btn-info my-2"
            urunkart.className = "col-3 offset-1 bg-light text-lg-center float-left mt-4 border border-warning rounded text-primary "


            // urunkart.setAttribute("class", "urunsira") <i class="fas fa-check-square"></i> 
            sepeteekle.setAttribute("onclick", "textsirala(" + i + ")")





            urunkart.innerHTML = selectadi + " , " + selectmarkasi + " , " + selectmodeli + " <hr>" + "Ürün fiyat  : " + selectfiyati + "<hr>" + "Ürün stok  : " + selectstogu + "<hr>";

            urunkart.appendChild(resim)
            urunalani.appendChild(urunkart);
            urunkart.appendChild(sepeteekle)

        }
    })
}

function textsirala(index) {
    secilenurunno = len.item(index).urunno
    document.getElementById("urunad").value = len.item(index).urunadi
    document.getElementById("urunmarka").value = len.item(index).urunmarkasi
    document.getElementById("urunmodel").value = len.item(index).urunmodeli
    document.getElementById("urunfiyat").value = len.item(index).urunfiyati
    document.getElementById("urunstok").value = len.item(index).urunstogu
    document.getElementById("imgfile").value = len.item(index).urunimg
        // b=len.item(index.urunimg)
        // İMGE ADINI GERİ YAZDIR
}



function urunsil() {
    // var yntrefdelete = new yntDeleteProduct();
    yonetimservice.deleteUrun(secilenurunno).then(function(res) {

        if (res) {
            alert(" ÜRÜN SİLME BAŞARILI")
        } else {
            alert(" HATALI ÜRÜN SİLME")
        }

    })
    urunsirala()
}





function urunguncelle() {
    // var yntrefupdate = new yntUpdateProduct()
    var ugadi = document.getElementById("urunad").value;
    var ugmarkasi = document.getElementById("urunmarka").value;
    var ugstogu = document.getElementById("urunstok").value;
    var ugmodeli = document.getElementById("urunmodel").value;
    var ugfiyati = document.getElementById("urunfiyat").value
    var ugimg = a

    yonetimservice.updateUrun(ugadi, ugmarkasi, ugstogu, ugmodeli, ugfiyati, ugimg, secilenurunno).then(function(res) {
        if (res) {
            alert(" BAŞARILI ÜRÜN GÜNCELLEME")
        } else {
            alert("HATALI ÜRÜN GÜNCELLEME")
        }

    })
    urunsirala()
    document.getElementById("urunad").value = "";
    document.getElementById("urunmarka").value = "";
    document.getElementById("urunstok").value = "";
    document.getElementById("urunmodel").value = "";
    document.getElementById("urunfiyat").value = "";
    document.getElementById("imgfile").value = "";



}






const tobase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

function basecevir() {



    const uploadedfile = document.getElementById("imgfile").files[0];

    tobase64(uploadedfile)
        .then(res => {
            console.log(res)
                // document.getElementById("base").innerHTML = res;
            a = res
        })
        .catch(err => {
            console.log(err)
        })



}