var Database_Name = 'eticaretdata';
var Version = 1.0;
var Text_Description = 'E-Ticaret Otomasyon proje';
var Database_Size = 2 * 1024 * 1024;
var veriler = openDatabase(Database_Name, Version, Text_Description, Database_Size);


class yonetimClass {
    // class yntCreatProductTable {
    creatUrunlerTablo() {

        var urun = new Promise(function(resolve) {

            veriler.transaction(function(calis) {
                calis.executeSql('CREATE TABLE IF NOT EXISTS UrunlerTablo(urunno INTEGER PRIMARY KEY,urunadi,urunmarkasi ,urunstogu,urunmodeli,urunfiyati,urunimg)', [],
                    function(tx, result) {
                        resolve(true)
                        console.log("tablo oluşturuldu")

                    },
                    function(tx, result) {
                        resolve(false)
                        console.log("tablo oluşturlmadı")
                    })
            })
        });
        return urun;

        // }
    }

    // class yntAddProduct {
    addUrun(urunad, urunmarka, urunstok, urunmodel, urunfiyat, urunimg) {

            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("INSERT INTO UrunlerTablo(urunadi,urunmarkasi,urunstogu,urunmodeli,urunfiyati,urunimg) VALUES(?,?,?,?,?,?)", [urunad, urunmarka, urunstok, urunmodel, urunfiyat, urunimg],
                        function(tx, result) {
                            resolve(true)
                            console.log("ürün eklendi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ürün başarısız ekleme")
                        })
                })
            });
            return urun;
        }
        // }

    // class yntUpdateProduct {
    updateUrun(urunadi, urunmarkasi, urunstogu, urunmodeli, urunfiyati, urunimg, urunno) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql("UPDATE UrunlerTablo  set urunadi=?,urunmarkasi=?,urunstogu=?,urunmodeli=?,urunfiyati=?,urunimg=? where urunno=?", [urunadi, urunmarkasi, urunstogu, urunmodeli, urunfiyati, urunimg, urunno],
                        function(tx, result) {
                            resolve(true)
                            console.log("ürün güncellendi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ürün başarısız guncel")
                        })
                })
            });
            return urun;




        }
        // }
        // class yntSelectProduct {

    selecturun() {

            var liste = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('SELECT *from  UrunlerTablo ', [],
                        function(tx, result) {

                            resolve(result.rows)
                        },
                        function(tx, error) {
                            resolve(false)
                            console.log("urun secimi başarısız")
                        })
                })
            });
            return liste;



        }
        // }

    // class yntDeleteProduct {
    deleteUrun(urunnosu) {
            var urun = new Promise(function(resolve) {

                veriler.transaction(function(tx) {
                    tx.executeSql('delete from UrunlerTablo where urunno=?', [urunnosu],
                        function(tx, result) {
                            resolve(true)
                            console.log("ürün silindi")

                        },
                        function(tx, result) {
                            resolve(false)
                            console.log("ürün başarısız silindi")
                        })
                })
            });
            return urun;

        }
        // }
}